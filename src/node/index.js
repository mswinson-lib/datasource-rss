const Parser = require('rss-parser');

exports.handler = async function(event, context, callback) {
  var url = event.url;

  if (url !== null) {
    var parser = new Parser();
    var feed = await parser.parseURL(url);

    callback(null, feed);
    return;
  } else {
    callback(null, {});
    return;
  }
}
